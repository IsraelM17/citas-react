import React, { Fragment, useState } from 'react';
import uuid from 'uuid/v4';
import PropTypes from 'prop-types';

const Formulario = ({crearCita}) => {

    const [cita, actualizarCita] = useState({
        mascota     : '',
        propietario : '',
        fecha       : '',
        hora        : '',
        sintomas    : ''
    });

    const [ error, actualizarError ] = useState(false);

    //Función que se ejecuta cada que el usuario escribe en un input
    /**
     * 
     * @param {*} e -> element
     */
    const handleChange = e => {
        actualizarCita({
            ...cita,
            [e.target.name] : e.target.value, 
        });
    }

    const { mascota, propietario, fecha, hora, sintomas } = cita;

    //Cuando el susuario presiona agregar cita
    const submitCita = e => {
        e.preventDefault();

        // Validar
        if(mascota.trim() === '' || propietario.trim() === '' || fecha.trim() === '' || hora.trim() === '' || sintomas.trim() === ''){
            actualizarError(true);
            return;
        }
        actualizarError(false);

        //Asignar un id
        cita.id = uuid();
 
        //Crear la cita
        crearCita(cita);

        //REiniciar el form
        actualizarCita({
            mascota     : '',
            propietario : '',
            fecha       : '',
            hora        : '',
            sintomas    : ''
        });
    }

    return ( 


        <Fragment>
            <h2>Crear cita</h2>

            { error ? <p className="alerta-error"> Todos los campos son obligatorios </p> : null }

            <form
                onSubmit={submitCita}
            >
                <label>Nombre Mascota</label>
                <input
                    type = "text"
                    name = "mascota"
                    className = "u-full-width"
                    placeholder = "Nombre Mascota"
                    onChange={handleChange}
                    value= {mascota}
                />
                <label>Nombre Dueño</label>
                <input
                    type = "text"
                    name = "propietario"
                    className = "u-full-width"
                    placeholder = "Nombre dueño"
                    onChange={handleChange}
                    value= {propietario}
                />
                <label>Fecha</label>
                <input
                    type = "date"
                    name = "fecha"
                    className = "u-full-width"
                    onChange={handleChange}
                    value= {fecha}
                />
                <label>Hora</label>
                <input
                    type = "Time"
                    name = "hora"
                    className = "u-full-width"
                    onChange={handleChange}
                    value= {hora}
                />
                <label>Sintomas</label>
                <textarea
                    className = "u-full-width"
                    name = "sintomas"
                    onChange={handleChange}
                    value= {sintomas}
                ></textarea>

                <button
                    type = "submit"
                    className = "u-full-width button-primary"
                > Agregar cita </button>

            </form>
        </Fragment>
     );
}

/**
 * Los Proptypes son una forma de documentar los componentes
 */
Formulario.propTypes = {
    crearCita : PropTypes.func.isRequired
}

export default Formulario;
